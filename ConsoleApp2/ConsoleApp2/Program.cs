﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
          string filename = @"..\..\Failinimi.txt";
            
            
            File.ReadAllLines(filename)
            .ToList()
            .ForEach(x => Console.WriteLine(x));
            
        }
    }
}
