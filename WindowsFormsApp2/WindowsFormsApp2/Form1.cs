﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            string filename = @"..\..\nimed.txt";
            File.ReadAllLines(filename)
                .ToList()
                .ForEach(x => this.panel1.Text = x);
        }
    }
}
