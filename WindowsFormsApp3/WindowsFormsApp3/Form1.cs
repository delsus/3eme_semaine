﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filename = @"..\..\nimed.txt";
            File.ReadAllLines(filename)
                               
                .Take(1)
                .ToList()
                .ForEach(x => this.Nimi.Text = x);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string filename = @"..\..\nimed.txt";
            File.ReadAllLines(filename)

                .Take(2)
                .ToList()
                .ForEach(x => this.label2.Text = x);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string filename = @"..\..\nimed.txt";
            File.ReadAllLines(filename)

                .Take(3)
                .ToList()
                .ForEach(x => this.label3.Text = x);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string filename = @"..\..\nimed.txt";
            File.ReadAllLines(filename)

                .Take(4)
                .ToList()
                .ForEach(x => this.label4.Text = x);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string filename = @"..\..\nimed.txt";
            File.ReadAllLines(filename)

                .Take(5)
                .ToList()
                .ForEach(x => this.label5.Text = x);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string filename = @"..\..\nimed.txt";
            File.ReadAllLines(filename)

                .Take(6)
                .ToList()
                .ForEach(x => this.label6.Text = x);
        }
    }
}
